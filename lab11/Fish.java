public class Fish extends Animal{
	private String waterType;

	// Constructor
	public Fish(String name, int leg, String waterType) {
		setName(name);
		setLeg(leg);
		// super(name, leg);
		setWaterType(waterType);
	}

	// Setter & Getter
	public String getWaterType() {
		return this.waterType;
	}

	public void setWaterType(String waterType) {
		if (waterType == "Fresh" || waterType == "Salt") {
			this.waterType = waterType;
		}
		else {
			this.waterType = "NULL";
		}
	}

	// Method
	public void Swim() {
		System.out.println(super.getName() + " is swimming.");
	}

	@Override
	public String toString() {
		return String.format("[%s, waterType=%s]", super.toString(), this.waterType);
	}
}
