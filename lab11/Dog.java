public class Dog extends Animal {
	// Constructor
	public Dog(String name, int leg) {
		super(name, leg);
	}

	// Method
	public void Run() {
		System.out.println(super.getName() + " is running.");
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
