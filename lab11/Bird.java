public class Bird extends Animal{

	// Constructor
	public Bird(String name, int leg) {
		super(name, leg);
	}

	// Method
	public void Fly() {
		System.out.println(super.getName() + " is flying.");
	}

	public void Sing() {
		System.out.println(super.getName() + " is singing.");
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
