public class Animal {
	private String name;
	private int leg;
	private int	meter;

	// Constructor
	public Animal()
	{
	}
	public Animal(String name, int leg) {
		this.name = name;
		this.leg = leg;
	}

	// Setter & Getter
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLeg() {
		return this.leg;
	}

	public void setLeg(int leg) {
		this.leg = leg;
	}

	public int getMeter() {
		return this.meter;
	}

	public void setMeter(int meter) {
		this.meter = meter;
	}

	// Method
	public void Eat() {
		System.out.println(name + " is eating.");
	}

	@Override
	public String toString() {
		return "[name=" + name + ", leg=" + leg + "]";
	}
}
