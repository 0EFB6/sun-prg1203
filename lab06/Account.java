public class Account {
    private double balance;
    private static double total = 0.0;

    // Constructor
    public Account(double initialBalance)
    {
        if (initialBalance < 0)
            balance = 0.0;
        else
            balance = initialBalance;
        increaseTotal(balance);
    }

    // Methods
    public void credit(double amount)
    {
        if (amount > 0)
        {
            balance += amount;
            increaseTotal(amount);
        }
    }

    public void debit(double amount)
    {
        if (amount > 0 && amount <= balance)
        {
            balance -= amount;
            decreaseTotal(amount);
        }
        else if (amount <= 0)
            System.out.println("Debit amount cannot be negative or zero");
        else
            System.out.println("Debit amount exceeded account balance");        
    }

    public static void increaseTotal(double amount)
    {
        total += amount;
    }

    public static void decreaseTotal(double amount)
    {
        total -= amount;
    }

    // Getter
    public double getBalance()
    {
        return balance;
    }

    public static double getTotal()
    {
        return total;
    }
}