import java.util.Scanner;

public class AccountTest
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        Account account1 = new Account(50.00);
        Account account2 = new Account(8.93);

        System.out.printf("account1 balance: $%.2f%n", account1.getBalance());
        System.out.printf("account2 balance: $%.2f%n", account2.getBalance());

        System.out.print("Enter amount to be deposited for account1: ");
        double deposit1 = input.nextDouble();
        account1.credit(deposit1);

        System.out.print("Enter amount to be deposited for account2: ");
        double deposit2 = input.nextDouble();
        account2.credit(deposit2);

        System.out.printf("account1 balance: $%.2f%n", account1.getBalance());
        System.out.printf("account2 balance: $%.2f%n", account2.getBalance());

        System.out.print("Enter amount to be withdrawn for account1: ");
        double withdrawal1 = input.nextDouble();
        account1.debit(withdrawal1);

        System.out.print("Enter amount to be withdrawn for account2: ");
        double withdrawal2 = input.nextDouble();
        account2.debit(withdrawal2);

        System.out.printf("account1 balance: $%.2f%n", account1.getBalance());
        System.out.printf("account2 balance: $%.2f%n", account2.getBalance());

        System.out.printf("Total balance of all accounts is: $%.2f%n", Account.getTotal());

        input.close();
    }
}