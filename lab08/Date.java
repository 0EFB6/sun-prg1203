class Date {
    private int day;
    private int month;
    private int year;
    private static final String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    // Constructor
    public Date()
    {
        day = 1;
        month = 1;
        year = 2000;
    }

    public Date(int dd, int mm, int yyyy)
    {
        setMonth(mm);    
        setDay(dd);
        setYear(yyyy);
    }

    public Date(String mm, int dd, int yyyy)
    {
        convertFromMonthName(mm);
        setDay(dd);
        setYear(yyyy);
    }

    public Date(int ddd, int yyyy)
    {
        setYear(yyyy);
        convertFromDayOfYear(ddd);
    }

    // Setters
    public void setDay(int dd)
    {
        if (dd > 0 && dd <= daysInMonth())
        {
            day = dd;
        }
        else
        {
            throw new IllegalArgumentException("Invalid day value");
        }
    }

    public void setMonth(int mm)
    {
        if (mm > 0 && mm <= 12)
        {
            month = mm;
        }
        else
        {
            throw new IllegalArgumentException("Invalid month value");
        }
    }

    public void setYear(int yyyy) {
        if (yyyy >= 1900 && yyyy <= 2100)
        {
            year = yyyy;
        }
        else
        {
            throw new IllegalArgumentException("Invalid year value");
        }
    }

    // Other Methods
    private void convertFromMonthName(String monthName)
    {
        for (int i = 0; i < monthNames.length; i++)
        {
            if (monthNames[i].equalsIgnoreCase(monthName))
            {
                setMonth(i + 1);
                return;
            }
        }
        setMonth(1);
    }

    private int daysInMonth()
    {
        if (leapYear() && month == 2)
        {
            return 29;
        }
        else
        {
            return monthDays[month - 1];
        }
    }

    private Boolean leapYear()
    {
        return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
    }

    private void convertFromDayOfYear(int ddd)
    {
        if (ddd >= 1 && ddd <= 365)
        {
            int monthIndex = 0;
            while (ddd > monthDays[monthIndex])
            {
                ddd -= monthDays[monthIndex];
                monthIndex++;
            }
            if (leapYear())
            {
                ddd--;
            }
            setMonth(monthIndex + 1);
            setDay(ddd);
        }
        else
        {
            setDay(1);
            setMonth(1);
        }
    }

    private int convertToDayOfYear()
    {
        int totalDays = day;
        for (int i = 1; i < month; i++)
        {
            totalDays += monthDays[i - 1];
        }
        if (leapYear() && month > 2)
        {
            totalDays++; // Add one day for leap year if the month is after February
        }
        return totalDays;
    }

    // Output methods
    public String toString()
    {
        return String.format("DD/MM/YYYY: %02d/%02d/%04d", day, month, year);
    }

    public String toMonthNameDateString()
    {
        return String.format("Month DD, YYYY: %s %02d, %04d", monthNames[month - 1], day, year);
    }

    public String toDayDateString()
    {
        return String.format("DDD YYYY: %03d %04d", convertToDayOfYear(), year);
    }
}