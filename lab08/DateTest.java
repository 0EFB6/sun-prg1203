import java.util.Scanner;

public class DateTest
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int choice;
        do
        {
            choice = getMenuChoice(scanner);

            switch (choice)
            {
                case 1:
                    createAndDisplayDate(choice, scanner);
                    break;
                case 2:
                    createAndDisplayDate(choice, scanner);
                    break;
                case 3:
                    createAndDisplayDate(choice, scanner);
                    break;
                case 4:
                    System.out.println("Exiting the program. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please enter a number between 1 and 4.");
            }
        } while (choice != 4);
    }

    private static int getMenuChoice(Scanner scanner)
    {
        System.out.println("Enter 1 for format (DD/MM/YYYY)");
        System.out.println("Enter 2 for format (MonthName DD, YYYY)");
        System.out.println("Enter 3 for format (DDD YYYY)");
        System.out.println("Enter 4 to exit");
        System.out.print("Choice: ");
        return scanner.nextInt();
    }

    private static void createAndDisplayDate(int choice, Scanner scanner)
    {
        Date date = null;
        int day, month, year, dayOfYear;
        String monthName;

        switch (choice)
        {
            case 1:
                System.out.print("Enter Day of Month: ");
                day = scanner.nextInt();
                System.out.print("Enter Month (1-12): ");
                month = scanner.nextInt();
                System.out.print("Enter Year: ");
                year = scanner.nextInt();
                date = new Date(day, month, year);
                break;
            case 2:
                System.out.print("Enter Month Name: ");
                monthName = scanner.next();
                System.out.print("Enter Day of Month: ");
                day = scanner.nextInt();
                System.out.print("Enter Year: ");
                year = scanner.nextInt();
                date = new Date(monthName, day, year);
                break;
            case 3:
                System.out.print("Enter Day of Year: ");
                dayOfYear = scanner.nextInt();
                System.out.print("Enter Year: ");
                year = scanner.nextInt();
                date = new Date(dayOfYear, year);
                break;
        }

        if (date != null)
        {
            System.out.println("\n" + date.toString());
            System.out.println(date.toMonthNameDateString());
            System.out.println(date.toDayDateString() + "\n");
        }
    }
}
