public class Product {
    // Instance variables
    private String title;
    private double price;
    private int qty;

    // Constructors
    public Product() {
        title = "";
        price = 0.0;
        qty = 0;
    }

    public Product(String title, double price, int qty) {
        this.title = title;
        this.price = price;
        this.qty = qty;
    }

    // Setters
    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    // Getters
    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    // toString method for a string representation of the object
    @Override
    public String toString() {
        return "Product{" +
                "title='" + title + '\'' +
                ", price=" + price +
                ", qty=" + qty +
                '}';
    }
}