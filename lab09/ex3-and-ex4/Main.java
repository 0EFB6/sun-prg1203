public class Main {
    public static void main(String[] args) {
        // Create a product using the parameterized constructor
        Product product1 = new Product("Laptop", 999.99, 5);

        // Print product information using the toString method
        System.out.println(product1);

        // Update product information using setter methods
        product1.setQty(10);
        product1.setPrice(899.99);

        // Print updated product information
        System.out.println(product1);
    }
}