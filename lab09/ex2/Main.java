import Package1.Class1;
import Package1.Class2;
import Package2.Class3;
import Package2.Class4;


// Main.java
public class Main {
    public static void main(String[] args) {
        Class2.setValueToVariables();
        Class3.setValueToVariables();
        Class4.setValueToVariables();
    }
}
