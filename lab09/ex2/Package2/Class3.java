// Package2/Class3.java
package Package2;

import Package1.Class1;

public class Class3 {
    public static void setValueToVariables() {
        // Accessing Class1 variables from Class3 in a different package
        Class1.publicVariable = 10;
        System.out.println("Class3.publicVariable = " + Class1.publicVariable);
        // Unable to access defaultVariable and protectedVariable directly in Class3
        // Class1.defaultVariable = 10; // This line will result in a compilation error
        // Class1.protectedVariable = 10; // This line will result in a compilation error
    }
}
