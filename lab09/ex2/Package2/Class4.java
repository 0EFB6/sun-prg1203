// Package2/Class4.java
package Package2;

import Package1.Class1;

public class Class4 extends Class1 {
    public static void setValueToVariables() {
        // Accessing Class1 variables from Class4, which is a subclass in a different package
        publicVariable = 10;
        // Unable to access privateVariable and defaultVariable directly in Class4
        // privateVariable = 10; // This line will result in a compilation error
        // defaultVariable = 10; // This line will result in a compilation error
    }
}
