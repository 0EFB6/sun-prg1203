// Package1/Class2.java
package Package1;

public class Class2 {
    public static void setValueToVariables() {
        // Accessing Class1 variables from Class2 in the same package
        Class1.publicVariable = 10;
        Class1.setDefaultVariable(10);
        Class1.setProtectedVariable(10);

        // Unable to access privateVariable directly in Class2
        // Class1.privateVariable = 10; // This line will result in a compilation error
    }
}
