// Package1/Class1.java
package Package1;

public class Class1 {
    private static int privateVariable;
    public static int publicVariable;
    static int defaultVariable; // package-private (default) access
    protected static int protectedVariable;

    // Getter and setter methods for demonstration purposes
    public static int getPrivateVariable() {
        return privateVariable;
    }

    public static void setPrivateVariable(int value) {
        privateVariable = value;
    }

    public static int getDefaultVariable() {
        return defaultVariable;
    }

    public static void setDefaultVariable(int value) {
        defaultVariable = value;
    }

    public static int getProtectedVariable() {
        return protectedVariable;
    }

    public static void setProtectedVariable(int value) {
        protectedVariable = value;
    }
}
