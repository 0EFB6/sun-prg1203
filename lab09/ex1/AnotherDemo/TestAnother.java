import com.prg1203.package1.ClassA;
import com.prg1203.package2.ClassB;

public class TestAnother {
    public static void main(String[] args) {
        // Testing ClassA and ClassB from different projects
        ClassA objA = new ClassA();
        objA.display();

        ClassB objB = new ClassB();
        objB.display();
    }
}
