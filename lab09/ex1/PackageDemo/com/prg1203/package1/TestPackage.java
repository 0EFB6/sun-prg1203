package com.prg1203.package1;

public class TestPackage {
    public static void main(String[] args) {
        ClassA objA = new ClassA();
        objA.display();

        // Testing ClassB (from a different package)
        com.prg1203.package2.ClassB objB = new com.prg1203.package2.ClassB();
        objB.display();
    }
}