# To Compile AnotherDemo

```
cd ex1/
javac -cp PackageDemo/bin -d . AnotherDemo/TestAnother.java
java -cp .:PackageDemo/bin TestAnother
```

# To Compile PackageDemo

```
cd PackageDemo/com/prg1203/package1
javac -d ../../../bin/ ClassA.java

cd PackageDemo/com/prg1203/package2
javac -d ../../../bin/ ClassB.java

cd PackageDemo/
javac -d bin/ com/prg1203/package1/TestPackage.java

cd ex1/
java -cp PackageDemo/bin com.prg1203.package1.TestPackage
```