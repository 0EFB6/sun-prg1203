
public class lecture_week4_4 {
    public static void main ( String[] args)
    {
        System.out.println(factorial(3));
    }

    public static int factorial(int n)
    {
        if (n == 1) // base case
            return 1;
        else
            return factorial(n - 1) * n; // 5! = 4! * 5
    }
}