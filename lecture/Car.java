import java.util.Scanner;

public class Car {
    private String colour;

    public Car()
    {
        colour = "null";
    }

    public void setColour(String c)
    {
        if (c.equals("black"))
        {
            System.out.println("Error cannot set to black.");
        }
        else
            colour = c;
    }

}