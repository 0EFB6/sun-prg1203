package week9;

public class Car {
    private String colour;
    private int speed;
    private Wheel frontWheel;


    // Constructors
    public Car()
    {
        this(0, "null", 0)
    }

    public Car(int s, String c, int w)
    {
        setSpeed(s);
        setColour(c);
        setFrontWheel(new Wheel(w));
    }

    // Setters and getters
    public String getColour() {
        return this.colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Wheel getFrontWheel() {
        return this.frontWheel;
    }

    public void setFrontWheel(Wheel frontWheel) {
        this.frontWheel = frontWheel;
    }

    // Other method
    public void drive()
    {
        System.out.println("Driving at " + speed + " km/h");
    }

    public void stop()
    {
        System.out.println("Stopped");
        speed = 0;
    }

    // toString
    public String toString()
    {
        return "Car: " + colour + ", " + speed + " km/h, " + frontWheel;
    }
}