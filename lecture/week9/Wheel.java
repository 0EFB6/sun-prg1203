package week9;

public class Wheel {
    private int size;

    // Constructors
    public Wheel()
    {
        size = 0;
    }

    public Wheel(int s)
    {
        setSize(s);
    }

    // Setters and getters
    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    // toString
    public String toString()
    {
        return "Wheel: " + size + " inches";
    }

}
