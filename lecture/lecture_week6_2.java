import java.util.Random;

public class lecture_week6_2
{
    public static void main(String[] args)
    {
        int dice;
        double num;
        boolean isEligible;

        // Using Math random method
        /*
        for (int i = 0; i < 10; i++)
        {
            num = Math.random();
            System.out.println("num is: " + num);
        }
        */

        // Option 2: using Random class
        Random rand = new Random();
        for (int i = 0; i < 50; i++)
        {
            dice = 1 + rand.nextInt(6);
            System.out.println("Dice: " + dice);
        }
    }
}