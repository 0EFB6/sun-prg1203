public class Account {
    private int balance; // Instance Variable
    private static int total; // Class Variable
    public static double fixdepositRate = 3.5; // Class Variable

    public void setBalance(int b) // Instance  Method / Non-Static Method
    {
        balance = b;
        total = b;
    }

    public static void setTotal(int t)
    {
        total = t;
    }
    public static int getTotal()
    {
        return total;
    }
}
