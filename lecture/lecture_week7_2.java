import java.util.Scanner;

public class lecture_week7_2
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);


        Student s1 = new Student();

        try {
            s1.setID("1234");
            s1.setSemester(143);

            for (int i = 0; i < 15; i++)
            {
                s1.setSemester(i);
            }
        }
        catch (IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
            System.out.print("Key in the reason: ");
            String reason = input.nextLine();
            System.out.println("The reason: " + reason);
        }
        System.out.println(s1);
    }
}