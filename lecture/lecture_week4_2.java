import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Formatter;

public class lecture_week4_2 {
    public static void main ( String[] args)
    {
        Formatter output;
        Scanner input = new Scanner(System.in);

        //String name;
        //float mark;
        
        //System.out.print("Enter name: ");
        //name = input.next();
        //System.out.print("Enter mark: ");
        //mark = input.nextFloat();


        try{
            output = new Formatter("clients.txt");
            output.format("%d %s %.2f\n", 1, "Alex", 500.00);
            output.format("%d %s %.2f\n", 2, "Richard", 100.00);
            output.format("%d %s %.2f\n", 3, "Sam", 80.00);
            //output.format("%s %.2f\n", name, mark);

            if (output != null)
                output.close();
            
        }
        catch (SecurityException e)
        {
            System.out.println("You do not have write access!");
            System.exit(1);
        }
        catch (FileNotFoundException fe)
        {
            System.out.println("Enter integer only!!!");
        }
        finally
        {
            System.out.println("Finally block always executed");
        }
        input.close();
    }
}