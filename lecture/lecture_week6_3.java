import java.util.Random;

public class lecture_week6_3
{
    public static void main(String[] args)
    {
        System.out.println("Max of 10, 20 is:" + calcMax(10,20));
        System.out.println("Max of 10, 20, 30 is:" + calcMax(10,20,30));

       
    }

    public static int calcMax(int a, int b)
    {
        if (a > b)
            return a;
        else
            return b;
    }

    public static int calcMax(int a, int b, int c)
    {
        if (a > b && a > c)
            return a;
        else if (b > a && b > c)
            return b;
        else
            return c;
    }


}