import java.util.Scanner;

public class lecture_week4_5 {
    public static void main ( String[] args)
    {
        int num;

        Scanner input = new Scanner(System.in);
        System.out.printf("Enter the number of disks:");
        num = input.nextInt();
        System.out.printf("The sequence of moves involved in the Tower of Hanoi are:\n");
        towers(num, 'A', 'C', 'B');
    }

    public static void towers(int num, char frompeg, char topeg, char auxpeg)
    {
        if (num == 1) // base case
        {
            System.out.printf("\n Move disk 1 from peg %c to peg %c", frompeg, topeg);
            return;
        }
        towers(num - 1, frompeg, topeg, auxpeg);
        System.out.printf("\n Move disk %d from peg %c to peg %c", num, frompeg, topeg);
        towers(num - 1, auxpeg, topeg, frompeg);
    }
}