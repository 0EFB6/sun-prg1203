public class Student {
    private String ID;
    private int semester;
    private static int total;

    public Student()
    {
        total++;
    }

    public static int getTotal()
    {
        return total;
    }

    public void setID(String i)
    {
        ID = i;
    }

    public void setSemester(int s)
    {
        if (s > 9)
            throw new IllegalArgumentException("Exceeded Semester");
            //System.out.println("Exceeded Semester");
        else
            semester = s;
    }

    public String toString()
    {
        return String.format("ID is: %s, semester is %d\n", ID, semester);
    }
}