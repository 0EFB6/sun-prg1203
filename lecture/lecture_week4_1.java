import java.util.InputMismatchException;
import java.util.Scanner;

public class lecture_week4_1 {
    public static void main ( String[] args)
    {
        Scanner input = new Scanner(System.in);

        int numerator, demoninator, result;
        int[] numbers = new int[2];

        try{
            System.out.println("Enter integer numberator: ");
            numerator = input.nextInt();
            System.out.println("Enter integer denominator: ");
            demoninator = input.nextInt();
            result = numerator / demoninator;
            //numbers[2] = 10;
            System.out.printf("Result is %d\n", result);
        }
        catch (ArithmeticException e)
        {
            System.out.println("Cannot divide by zero!");
        }
        catch (InputMismatchException e)
        {
            System.out.println("Enter integer only!!!");
        }
        catch (Exception e)
        {
            System.out.println("Error found: " + e);
        }
        finally
        {
            System.out.println("Finally block always executed");
        }

        input.close();
    }
}