import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class lecture_week4_3 {
    public static void main ( String[] args)
    {
        Scanner input;

        try{
            input = new Scanner(new File("clients.txt"));
            while (input.hasNext())
            {
                //int row = input.nextInt();
                //String name = input.next();
                //double mark = input.nextDouble();
                System.out.printf("%s %s %s\n", input.nextInt(), input.next(), input.nextDouble());
            }

            if (input != null)
                input.close();
            
        }
        catch (FileNotFoundException fe)
        {
            System.out.println("Enter integer only!!!");
        }
        //input.close();
    }
}