import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;

public class lecture_week2_2 {
	public static void main( String[] args ) {
		double result;
		printWelcome();
		printHello("Wilson");

		result = getLargest(10,5,30);
		System.out.printf("Largest is %f \n", result);


		int[] alist = new int [10];

		for(int i = 0; i < alist.length; i++) {
			alist[i] = i * 10;
		}

		System.out.println(Arrays.toString(alist));

		alist[2] = 69;

		for(int i = 0; i < alist.length; i++) {
			System.out.println("Element " + i + " is " + alist[i]);
		}

		for (int a : alist) {
			System.out.println(a);
		}

		ArrayList<Integer> blist = new ArrayList<Integer>();

		for(int i = 0; i < 10; i++) {
			blist.add(i * 10);
		}

		System.out.println(blist);

		for(int i = 0; i < blist.size(); i++) {
			System.out.println(blist.get(i));
		}

		blist.add(69);
		System.out.println(blist);
		blist.remove(3);
		System.out.println(blist);
	}
	
	public static void printWelcome() {
		System.out.println("Welcome to PRG1203");
	}

	public static void printHello(String name) {
		System.out.println("Hello " + name);
	}

	public static double getPi() {
		return 3.14159;
	}

	public static double getLargest(int a, int b, int c) {
		int largest = a;
		if (b > largest) largest = b;
		if (c > largest) largest = c;
		return largest;
	}
}
