import java.util.Scanner;

public class hellojava { // class header

	public static void main(String[] args) { // method header
		Scanner mykeyboard = new Scanner(System.in);
		// declare the variable
		int n = 0;
		String id = "12";
		String name;
		float mark;
		double price;
		char grade;
		boolean isEligible;
		
		// initialize
		name = "wilson";
		mark = 10.0f;
		price = 20.0;
		grade = 'A';
		isEligible = false;
		n = Integer.parseInt(id);
		price = n;
		n = (int)price; // casting: temporary change the data type

		// implementation
		price = n / 5;


		int num = 5;
		int num1, num2, num3;
		double result;

		result = (double)num / 2; // if two operands are integers, the result will automatically be in integer
		System.out.println("Result is " + result);
		System.out.print("hey you\n");
		System.out.printf("this is powerful");

		int no;

		System.out.println("Enter a number:");
		no = mykeyboard.nextInt();

		System.out.println("Enter name:");
		name = mykeyboard.next();
		System.out.printf("Hello %s, you entered %d\n", name, no);
	}

}
