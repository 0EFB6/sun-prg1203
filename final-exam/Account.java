import java.util.ArrayList;

public class Account {
	private String name;
	private String email;
	private String password;
	private ArrayList<Room> rooms;

	// Constructors
	public Account() {
	}

	public Account(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.rooms = new ArrayList<Room>();
	}

	// Getters
	public String getName() {
		return this.name;
	}

	public String getEmail() {
		return this.email;
	}

	public String getPassword() {
		return this.password;
	}

	public ArrayList<Room> getRooms() {
		return this.rooms;
	}

	// Setters
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		if (password.length() > 8)
			this.password = password;
		else
			System.out.println("Password not set!!!");
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}

	// Methods
	@Override
	public String toString() {
		return String.format("[Account] Name: %s, Email: %s\n", name, email);
	}

	public void	registerAccount(String name, String email, String password) {
		setName(name);
		setEmail(email);
		setPassword(password);
		setRooms(new ArrayList<Room>());
	}

	public void	addRoom(String roomName) {
		rooms.add(new Room(rooms.size() + 1, roomName));
	}

	public void	deleteRoom(int roomIndex) {
		rooms.remove(roomIndex);
	}

	public void printRooms() {
		for (Room room: rooms) {
			System.out.print(room);
		}
		System.out.println();
	}

	public Room	getRoomByID(int roomID) {
		if (roomID >= 1)
			return this.rooms.get(roomID - 1);
		return null;
	}
}