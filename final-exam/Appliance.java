public class Appliance {
	private int applianceID;
	private String applianceName;
	private String status;

	// Constructors
	public Appliance() {
	}

	public Appliance(int applianceID, String applianceName, String status) {
		this.applianceID = applianceID;
		this.applianceName = applianceName;
		setStatus(status);
	}

	// Getters
	public int getApplianceID() {
		return this.applianceID;
	}

	public String getApplianceName() {
		return this.applianceName;
	}

	public String getStatus() {
		return this.status;
	}

	// Setters
	public void setApplianceID(int applianceID) {
		if (applianceID > 0)
			this.applianceID = applianceID;
		else
			System.out.println("Fail to set appliance ID!");
	}

	public void setApplianceName(String applianceName) {
		this.applianceName = applianceName;
	}

	public void setStatus(String s) {
		if (s.equalsIgnoreCase("NON-ACTIVE"))
			this.status = s;
		else if (s.equalsIgnoreCase("ACTIVE"))
			this.status = s;
		else
			this.status = null;
	}

	// Methods
	@Override
	public String toString() {
		return String.format("[Appliance] ID: A%d, Name: %s, Status: %s", applianceID, applianceName, status);
	}

	public void	onAppliance() {
		setStatus("ACTIVE");
	}

	public void	offAppliance() {
		setStatus("NON-ACTIVE");
	}
}