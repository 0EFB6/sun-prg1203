import java.util.ArrayList;

public class Driver {
	public static void main(String[] args) {
		Account user1 = new Account();
		user1.registerAccount("Ben", "ben@mail.com", "123456789");
		System.out.println(user1);

		user1.addRoom("Living Room");
		user1.addRoom("Kitchen");
		user1.printRooms();

		System.out.println("=================Showing Room 1 Below =================\n");

		user1.getRoomByID(1).addAppliance(new CoolingAppliance(1, "Fan", "NON-ACTIVE", 0));
		user1.getRoomByID(1).addAppliance(new LightingAppliance(2, "Study Lamp", "NON-ACTIVE", 0));
		user1.getRoomByID(1).addAppliance(3, "Television");
		
		user1.getRoomByID(1).printAppliances();
		
		user1.getRoomByID(1).turnOn(1);
		user1.getRoomByID(1).turnOn(2);
		user1.getRoomByID(1).turnOn(3);
		
		user1.getRoomByID(1).printAppliances();

		user1.getRoomByID(1).increaseSpeed(1);
		user1.getRoomByID(1).increaseSpeed(1);
		user1.getRoomByID(1).increaseSpeed(1);
		user1.getRoomByID(1).increaseBrightness(2);
		user1.getRoomByID(1).increaseBrightness(2);
		user1.getRoomByID(1).increaseBrightness(2);

		user1.getRoomByID(1).printAppliances();

		user1.getRoomByID(1).decreaseSpeed(1);
		user1.getRoomByID(1).decreaseBrightness(2);
		user1.getRoomByID(1).decreaseBrightness(2);
		user1.getRoomByID(1).turnOff(3);

		user1.getRoomByID(1).printAppliances();
		
		System.out.println("=================Showing Room 2 Below =================\n");

		user1.getRoomByID(2).addAppliance(4, "Rice Cooker");
		
		user1.getRoomByID(2).printAppliances();

		user1.getRoomByID(2).turnOn(1);

		user1.getRoomByID(2).printAppliances();		
	}
}