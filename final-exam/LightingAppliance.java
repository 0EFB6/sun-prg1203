public class LightingAppliance extends Appliance {
	private int	brightness;

	// Constructor
	public LightingAppliance() {
	}

	public LightingAppliance(int applianceID, String applianceName, String status, int brightness)
	{
		super(applianceID, applianceName, status);
		setBrightness(brightness);
	}

	// Getter
	public int getBrightness() {
		return this.brightness;
	}

	// Setter
	public void setBrightness(int brightness) {
		if (brightness >= 0)
			this.brightness = brightness;
		else
			System.out.println("Fail to set brightness!");
	}

	// Methods
	@Override
	public String toString() {
		return String.format("[LightningAppliance] %s, Brightness: %s", super.toString(), brightness);
	}

	public void increaseBrightness() {
		this.brightness += 10;
	}

	public void decreaseBrightness() {
		if (this.brightness > 9)
			this.brightness -= 10;
	}
}