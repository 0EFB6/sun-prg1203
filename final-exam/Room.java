import java.util.ArrayList;

public class Room {
	private int roomID;
	private String roomName;
	private ArrayList<Appliance> appliances;

	// Constructors
	public Room() {
	}

	public Room(int roomID, String roomName) {
		setRoomID(roomID);
		this.roomName = roomName;
		this.appliances = new ArrayList<Appliance>();
	}

	// Getters
	public int getRoomID() {
		return this.roomID;
	}

	public String getRoomName() {
		return this.roomName;
	}

	public ArrayList<Appliance> getAppliances() {
		return this.appliances;
	}

	// Setters
	public void setRoomID(int roomID) {
		if (roomID > 0)
			this.roomID = roomID;
		else
			System.out.println("Fail to set room ID!");
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public void setAppliances(ArrayList<Appliance> appliances) {
		this.appliances = appliances;
	}

	// Methods
	@Override
	public String toString() {
		return String.format("[Room] Room ID: R%d, Room Name: %s, Number of Appliances: %d\n", roomID, roomName, getNumberOfAppliances());
	}

	public int getNumberOfAppliances() {
		return appliances.size();
	}

	public void addAppliance(int applianceID, String name) {
		
		appliances.add(new Appliance(applianceID, name, "NON-ACTIVE"));
	}

	public void addAppliance(Appliance a) {	
		appliances.add(a);
	}

	public void deleteAppliance(int applianceID) {
		if (applianceID > 0 && applianceID <= appliances.size())
			appliances.remove(applianceID - 1);
		else
			System.out.println("Fail to delete appliance with ID " + applianceID);
	}

	public void printAppliances() {
		System.out.print(toString());
		for (Appliance a: appliances) {
			System.out.println(a);
		}
		System.out.println();
	}

	public void	turnOn(int id) {
		if (id >= 1 && id <= appliances.size())
			appliances.get(id - 1).onAppliance();
		else
			System.out.println("Fail to turn on appliance with ID " + id);
	}

	public void	turnOff(int id) {
		if (id >= 1 && id <= appliances.size())
			appliances.get(id - 1).offAppliance();
		else
			System.out.println("Fail to turn off appliance with ID " + id);
	}

	public void	increaseSpeed(int id) {
		if (id >= 1 && id <= appliances.size() && appliances.get(id - 1) instanceof CoolingAppliance)
		{
			CoolingAppliance tmp = null;
			tmp = (CoolingAppliance) appliances.get(id - 1);
			tmp.increaseSpeedAppliance();
		}
		else
			System.out.println("Fail to increase speed of appliance with ID " + id);
	}

	public void	decreaseSpeed(int id) {
		if (id >= 1 && id <= appliances.size() && appliances.get(id - 1) instanceof CoolingAppliance)
		{
			CoolingAppliance tmp = null;
			tmp = (CoolingAppliance) appliances.get(id - 1);
			tmp.decreaseSpeedAppliance();
		}
		else
			System.out.println("Fail to decrease speed of appliance with ID " + id);
	}
	
	public void	increaseBrightness(int id) {
		if (id >= 1 && id <= appliances.size() && appliances.get(id - 1) instanceof LightingAppliance)
		{
			LightingAppliance tmp = null;
			tmp = (LightingAppliance) appliances.get(id - 1);
			tmp.increaseBrightness();
		}
		else
			System.out.println("Fail to increase brightness of appliance with ID " + id);
	}

	public void	decreaseBrightness(int id) {
		if (id >= 1 && id <= appliances.size() && appliances.get(id - 1) instanceof LightingAppliance)
		{
			LightingAppliance tmp = null;
			tmp = (LightingAppliance) appliances.get(id - 1);
			tmp.decreaseBrightness();
		}
		else
			System.out.println("Fail to decrease brightness of appliance with ID " + id);
	}
}