public class CoolingAppliance extends Appliance {
	private int	speed;

	// Constructor
	public CoolingAppliance() {
	}

	public CoolingAppliance(int applianceID, String applianceName, String status, int speed)
	{
		super(applianceID, applianceName, status);
		setSpeed(speed);
	}

	// Getter
	public int getSpeed() {
		return this.speed;
	}

	// Setter
	public void setSpeed(int speed) {
		if (speed >= 0)
			this.speed = speed;
		else
			System.out.println("Fail to set speed!");
	}

	// Methods
	@Override
	public String toString() {
		return String.format("[CoolingAppliance] %s, Speed: %s", super.toString(), speed);
	}

	public void	increaseSpeedAppliance() {
		this.speed += 1;
	}

	public void	decreaseSpeedAppliance() {
		if (this.speed > 0)
			this.speed -= 1;
	}
}