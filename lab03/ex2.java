import java.util.Scanner;
import java.util.Arrays;

public class ex2
{
    public static void main(String[] args)
	{
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[5];

        try 
		{
            for (int i = 0; i < 5; i++)
			{
                System.out.print("Enter an integer for element " + i + ": ");
                numbers[i] = scanner.nextInt();
            }
            //numbers[5] = 10;
            System.out.println("Numbers entered:");
            for (int i = 0; i < 5; i++)
			{
                System.out.println("Element " + i + ": " + numbers[i]);
            }
            // int result = numbers[0] / 0;
        }
		catch (java.util.InputMismatchException e)
		{
            System.out.println("Input Error: Please enter an integer.");
        }
		catch (ArrayIndexOutOfBoundsException e)
		{
            System.out.println("Array Index Error: You accessed an invalid array element.");
        }
		catch (ArithmeticException e)
		{
            System.out.println("Arithmetic Error: Division by zero is not allowed.");
        }

        scanner.close();
    }
}

