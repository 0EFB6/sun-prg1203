import java.util.Scanner;
import java.lang.Math;

public class ex1 {
    public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int n;
		int result;

		System.out.print("Enter a number: ");
		n = input.nextInt();
		result = sum(n);
		System.out.println("Sum of numbers from 1 to " + n + " is: " + result);
    }

    public static int sum(int n)
	{
        if (n == 1)
			return 1;
        else
			return n + sum(n - 1);
    }
}

