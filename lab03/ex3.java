import java.io.FileWriter;
import java.util.Scanner;

public class ex3 {
    public static void main ( String[] args)
    {
        FileWriter fw;
        Scanner input = new Scanner(System.in);

        String name;
        float mark;

        try{

            fw = new FileWriter("student_marks.txt");

            while (true)
            {
                System.out.print("Enter name (-1 to stop writing): ");
                name = input.next();
                if (name.equals("-1"))
                    break;
                System.out.print("Enter mark: ");
                mark = input.nextFloat();
                fw.write(name + " " + mark + "\n");
            }

            fw.close();
            input.close();
        }
        catch (SecurityException e)
        {
            System.out.println("You do not have write access!");
            System.exit(1);
        }
        catch (Exception e)
        {
            System.out.println("Error found: " + e);
        }
        finally
        {
            System.out.println("Done writing to file. If error stated, please check it yourself.");
        }
        input.close();
    }
}