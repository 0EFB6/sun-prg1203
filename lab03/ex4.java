import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ex4 {
    public static void main ( String[] args)
    {
        Scanner input;

        try{
            input = new Scanner(new File("student_marks.txt"));
            while (input.hasNext())
            {
                System.out.printf("%s %s\n", input.next(), input.nextDouble());
            }

            if (input != null)
                input.close();
            
        }
        catch (FileNotFoundException fe)
        {
            System.out.println("File not found!!!");
        }
    }
}