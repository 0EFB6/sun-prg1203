public class SupplierAccount extends Account{
	private String SupplierType;

	// Constructor
	public SupplierAccount(String number, String name, double balance, String SupplierType)
	{
		super(number, name, balance);
		this.SupplierType = SupplierType;
	}

	// Setter & Getter
	public String getSupplierType() {
		return this.SupplierType;
	}

	public void setSupplierType(String SupplierType) {
		if (SupplierType == "VIP" || SupplierType == "Normal")
			this.SupplierType = SupplierType;
		else
			this.SupplierType = "NULL";
	}

	// Method
	@Override
	public void increaseBalance(double amount) {
		if (SupplierType.equals("VIP"))
			super.increaseBalance(amount * 2);
		else
			super.increaseBalance(amount);
	}

	public void sellItem(int qty, double price) {
		super.increaseBalance(qty * price);
	}
	
	@Override
	public String toString() {
		return String.format("Supplier: type %s %s", SupplierType, super.toString() );
	}
}
