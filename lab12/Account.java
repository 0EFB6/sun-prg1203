public class Account {
	private String number;
	private String name;
	protected double balance;

	// Constructor
	public Account()
	{
	}

	public Account(String number, String name, double balance) {
		this.number = number;
		this.name = name;
		this.balance = balance;
	}

	// Setter & Getter
	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return this.balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void increaseBalance(double amount) {
		if (amount > 0)
			this.balance += amount;
	}

	public void decreaseBalance(double amount) {
		if (amount > 0)
			this.balance -= amount;
	}

	// Method
	@Override
	public String toString() {
		return String.format("account number %s, name %s, current balance is: %f", number, name, balance);
	}

}
