public class AccountTest {
    public static void main(String[] args) {
		Account acc1 = new Account("A001", "Pensonic", 0.0);
		SupplierAccount Sacc1 = new SupplierAccount("S001", "Jones",0.0,"VIP");
		SupplierAccount Sacc2 = new SupplierAccount("S002", "Albert",0.0,"Normal");
		CustomerAccount Cacc1= new CustomerAccount("C001", "Daniel",1000.0);
		Sacc1.sellItem(100, 5.80);
		Sacc2.sellItem(100, 5.80);
		Cacc1.buyItem(7, 250.30);
		Cacc1.ReceivePayment(200);
		System.out.println(acc1.toString());
		System.out.println(Sacc1.toString());
		System.out.println(Sacc2.toString());
		System.out.println(Cacc1.toString());
		System.out.println(acc1);
		System.out.println(Sacc1);
		System.out.println(Sacc2);
		System.out.println(Cacc1);
	}
}