public class CustomerAccount extends Account {
	// Constructor
	public CustomerAccount(String number, String name, double balance) {
        super(number, name, balance);
    }

	// Method
    public boolean checkBalance(double total) {
        return balance > total;
    }

    public void buyItem(int quantity, double price) {
        if (this.checkBalance(quantity * price)) {
            super.decreaseBalance(quantity * price);
        }
		else {
            System.out.println("Insufficient balance!");
        }
    }

    public void ReceivePayment(double amount) {
        super.increaseBalance(amount);
    }

	@Override
	public String toString() {
		return String.format("Customer: %s", super.toString());
	}
}
