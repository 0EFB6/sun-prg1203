public class Employee {
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Date hireDate;

    // Constructor
    public Employee ()
    {
        setFirstName("null");
        setLastName("null");
        birthDate = null;
        hireDate = null;
    }
    public Employee(String first, String last, int bd, int bm, int by, int hd, int hm, int hy)
    {
        setFirstName(first);
        setLastName(last);
        birthDate = new Date(bd, bm, by);
        hireDate = new Date(hd, hm, hy);
    }

    // Getters and Setters
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public Date getHireDate() {
        return this.hireDate;
    }



    // toString
    public String toString() {
        return String.format("Employee with first name %s last name %s has birthday on %s and was hired on %s",
                firstName, lastName, birthDate, hireDate);
    }
}
