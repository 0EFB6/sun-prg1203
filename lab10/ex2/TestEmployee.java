public class TestEmployee {
    public static void main(String[] args) {
        Employee e1 = new Employee();
        Employee e2 = new Employee("George", "Tan",17, 8, 1990, 25, 6, 2014);
        System.out.println(e1);
        System.out.println(e2);
        e2.setLastName("Chan");
        System.out.println(e2);
        e2.getHireDate().setYear(2015);
        System.out.println(e2);
    }
}