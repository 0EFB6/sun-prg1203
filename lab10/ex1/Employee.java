public class Employee {
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Date hireDate;

    // Constructor
    public Employee ()
    {
        this("John", "Doe", new Date(1, 1, 2000), new Date(1, 1, 2000));
    }
    public Employee(String first, String last, Date birth, Date hire)
    {
        firstName = first;
        lastName = last;
        birthDate = birth;
        hireDate = hire;
    }

    // Getters and Setters
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getHireDate() {
        return this.hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    // toString
    public String toString() {
        return String.format("Employee with first name %s last name %s has birthday on %s and was hired on %s",
                firstName, lastName, birthDate, hireDate);
    }
}
