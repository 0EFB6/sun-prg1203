import java.util.Arrays;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qty;

    // Constructors
    public Book (String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book (String name, Author[] authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }

    // Getters and Setters

    public String getName() {
        return this.name;
    }

    public Author[] getAuthors() {
        return this.authors;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return this.qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    // toString
    @Override
    public String toString() {
        return String.format("Book [name=%s, authors={%s}, price=%.2f, qty=%d]", name, Arrays.toString(getAuthors()), price, qty);
    }

    // Other Methods
    public String getAuthorNames() 
    {
        String authorNames = "";

        for (int i = 0; i < authors.length; i++)
            if (i == authors.length - 1)
                authorNames += this.authors[i].getName();
            else
                authorNames += this.authors[i].getName() + ", ";

        // for (Author a: authors)
        //     authorNames += a.getName() + ", ";

        return authorNames;
    }
}