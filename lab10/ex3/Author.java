public class Author {
    private String name;
    private String email;
    private char gender;

    // Constructor
    public Author(String n, String e, char g)
    {
        name = n;
        setEmail(e);
        gender = g;
    }

    // Setters and Getters
    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return this.gender;
    }

    // toString
    @Override
    public String toString() {
        return String.format("Author [name=%s, email=%s, gender=%c]", name, email, gender);
    }
}