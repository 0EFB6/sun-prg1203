public class Book {
    private String name;
    private Author author;
    private double price;
    private int qty;

    // Constructors
    public Book (String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book (String name, Author author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }

    // Getters and Setters

    public String getName() {
        return this.name;
    }

    public Author getAuthor() {
        return this.author;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return this.qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    // toString
    @Override
    public String toString() {
        return String.format("Book [name=%s, %s, price=%.2f, qty=%d]", name, author, price, qty);
    }

    // Other Methods
    public String getAuthorName() {
        return this.author.getName();
    }

    public String getAuthorEmail() {
        return this.author.getEmail();
    }

    public char getAuthorGender() {
        return this.author.getGender();
    }
}