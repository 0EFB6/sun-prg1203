import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class SortStudents_modified
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner(System.in);
		String name;
		ArrayList<String> names = new ArrayList<String>();

        // Allow user to add names into the Array List
		System.out.print("Enter name (-1 to end loop): ");
		name = input.next();
		while (!name.equals("-1"))
		{	
            if (!checkDuplicate(names, name))
                names.add(name);
            else
                System.out.println("Duplicate name found. Please enter another name.");
			System.out.print("Enter name (-1 to end loop): ");
			name = input.next();
		}

        Collections.sort(names);

		System.out.println("\nThe names you entered are: ");
		for(int i = 0; i < names.size() - 1; i++)
			System.out.print(names.get(i) + ",");
		System.out.println(names.get(names.size() - 1) + "\n");

        // Allow user to delete names from Array List
        System.out.print("Enter a name to delete (-1 to end loop): ");
        name = input.next();
        while (!name.equals("-1"))
        {
            if (checkDuplicate(names, name))
                names.remove(name);
            else
                System.out.println("Name not found. Please enter another name.");
            System.out.print("Enter a name to delete (-1 to end loop): ");
            name = input.next();
        }

        System.out.println("\nThe names after deleting are: ");
		for(int i = 0; i < names.size() - 1; i++)
			System.out.print(names.get(i) + ",");
		System.out.println(names.get(names.size() - 1) + "\n");

        for(String n: names)
            System.out.print(n + ",");

        // Allow user to search for names
        System.out.print("Enter a name to search (-1 to end loop): ");
        name = input.next();
        while (!name.equals("-1"))
        {
            int index = Collections.binarySearch(names, name);
            if (index >= 0)
                System.out.println("Name found at index " + index);
            else
                System.out.println("Name not found.");

            System.out.print("Enter a name to search (-1 to end loop): ");
            name = input.next();
        }

        // Close the scanner
		input.close();
	}

    public static boolean checkDuplicate( ArrayList<String> names, String name )
    {
        for (int i = 0; i < names.size(); i++)
            if (names.get(i).equals(name))
                return true;
        return false;
    }
}