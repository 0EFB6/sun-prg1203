import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collection;

public class SortStudents
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner(System.in);
		String name;
		ArrayList<String> names = new ArrayList<String>();

		System.out.print("Enter name (-1 to end loop): ");
		name = input.next();
		while (!name.equals("-1"))
		{	
			names.add(name);
			System.out.print("Enter name (-1 to end loop): ");
			name = input.next();
		}

		System.out.println("\nThe name you entered are: ");
		for(int i = 0; i < names.size() - 1; i++) {
			System.out.print(names.get(i) + ",");
		}
		System.out.println(names.get(names.size() - 1));
		input.close();
	}
}