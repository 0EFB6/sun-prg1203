import java.util.Scanner;
import java.util.Arrays;

public class ex1_modified
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner(System.in);
		int index;

		System.out.print("Enter the number of elements: ");
		index = input.nextInt();

		int input_num;
		int[] numbers = new int [index];
		
		index = 0;

		while (index < numbers.length)
		{
			System.out.print("Enter a number: ");
			input_num = input.nextInt();

			if (!isDuplicate(numbers, index, input_num))
			{
                numbers[index] = input_num;
                index++;
                printArray(numbers, index);
            }
			else
                System.out.println("Duplicate number. Please enter a different number.");
		}
		input.close();
	}

	public static boolean isDuplicate(int[] array, int currentIndex, int number)
	{
        for (int i = 0; i < currentIndex; i++)
            if (array[i] == number)
                return true;
        return false;
    }

    public static void printArray(int[] array, int index)
	{
		for (int i = 0; i < index; i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }
}