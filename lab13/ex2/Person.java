class Person {
	private String name;
	private String address;

	// Constructor
	public Person (String name, String address) {
		this.name = name;
		this.address = address;
	}

	// Getters and Setters
	public String getName() {
		return this.name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	// Other methods
	@Override
	public String toString() {
		return "Person[name=" + this.name + ", address=" + this.address + "]";
	}
}