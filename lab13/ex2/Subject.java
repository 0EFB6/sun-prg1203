public class Subject {
	private String subjectCode;
	private Student[] students;
    private Staff lecturer;

	// Constructor
	public Subject(String subjectCode, Staff lecturer, Student[] students) {
		this.subjectCode = subjectCode;
		this.lecturer = lecturer;
		this.students = students;
	}

	// Setters and getters
	public String getSubjectCode() {
		return this.subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public Student[] getStudents() {
        return students;
    }

    public Staff getLecturer() {
        return lecturer;
    }

	public void setLecturer(Staff lecturer) {
		this.lecturer = lecturer;
	}

	public String printStudents() {
		String students = "";
		for (Student student : this.students)
			students += student.toString() + "\n";
        return students;
    }

	// Other methods
	@Override
	public String toString() {
		return "Subject [subjectCode=" + this.subjectCode + "], Students=" + printStudents() + "], Lecturer=" + this.lecturer;
	}
}