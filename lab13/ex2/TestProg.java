import java.util.ArrayList;

public class TestProg {
    public static void main(String[] args) {
        Staff staff = new Staff("John Staff", "Sunway, Selangor", "Sunway University", 2500.00);
        System.out.println("Before updating school:");
        System.out.println(staff);
        staff.setSchool("Sunway College");
        System.out.println("After updating school:");
        System.out.println(staff);

        Staff lecturer = new Staff("Aung Pyae", "Subang, Sabah", "Sunway University", 30.00);
		Student[] students = new Student[3];
		students[0] = new Student("Brendon", "Melaka", "BCS", 1, 1000.3);
		students[1] = new Student("Cherry","Johor", "BCNS", 2, 10000.6);
		students[2] = new Student("Charlie","Penang", "BSE", 3, 69000.0);
        Subject subject = new Subject("PRG1203", lecturer, students);
		System.out.println("\nSubject:");
		System.out.println(subject);

        ArrayList<Student> studentList = new ArrayList<Student>();
        studentList.add(students[0]);
		studentList.add(students[1]);
		studentList.add(students[2]);
        System.out.println("\nStudents in the ArrayList:");
        for (Student stu : studentList) {
            System.out.println(stu);
        }

		ArrayList<Staff> lectureList = new ArrayList<Staff>();
        lectureList.add(lecturer);
        System.out.println("\nStaff in the ArrayList:");
        for (Staff lec : lectureList) {
            System.out.println(lec);
        }
    }
}