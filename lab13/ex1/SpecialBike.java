public class SpecialBike extends Bike {
	private String specification;

	// Constructor
	public SpecialBike() {
	}

	public SpecialBike(int id, String description, double price, String type, String specification) {
		super(id, description, price, type);
		setSpecification(specification);
	}

	// Setters and getters
	public String getSpecification() {
		return this.specification;
	}

	public void setSpecification(String specification) {
		if (specification.equals("lightweight") || specification.equals("middleweight") || specification.equals("superbike"))
			this.specification = specification;
		else
			this.specification = "NULL";
	}

	// Other Methods
	@Override
	public String toString() {
		return "SpecialBike [" + super.toString() + ", specification=" + this.specification + "]";
	}
}
