public class Bike {
	private int id;
	private String description;
	private double price;
	private String type;

	// Constructor
	public Bike() {
	}

	public Bike(int id, String description, double price, String type) {
		this.id = id;
		this.description = description;
		this.price = price;
		setType(type);
	}

	// Getters and setters
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		if (type.equals("normal") || type.equals("sports"))
			this.type = type;
		else
			this.type = "Invalid Type";
	}

	// Other Methods
	@Override
	public String toString() {
		return "[id=" + this.id + ", description=" + this.description + ", price=" + this.price + ", type="
				+ this.type + "]";
	}
}

