public class Test {
	public static void main (String[] args) {
		// Create a new customer
		Customer customer = new Customer(1, "John", "123 Main St.");
		// Create a new order
		// Order order = new Order(1, customer, "2020-01-01", null);
		// Create a new orderline
		// Orderline orderline = new Orderline(1, 1, 1, 1);
		// Create a new bike
		NormalBike bike = new NormalBike(1, "Bike", 100.0, "normal");
		// Create a new special bike
		SpecialBike specialBike = new SpecialBike(6, "Special Bike", 200.0, "sports", "lightweight");
		// Set the orderline's bike to the bike
		// orderline.setBike(bike);
		// Set the orderline's special bike to the special bike
		// orderline.setSpecialBike(specialBike);
		// Set the order's orderlines to the orderline
		// order.setOrderlines(new Orderline[] {orderline});
		// Print the order
		// System.out.println(order);

		// Array for Bike

		System.out.println(customer);
		System.out.println(bike);
		System.out.println(specialBike);


		Bike[] bikes = new Bike[3];
		for (int i = 0; i < bikes.length; i++) {
			bikes[i] = new NormalBike(i + 1, "Bike" + String.valueOf(i + 1), 100.0 + i, "normal");
		}
		for (Bike b: bikes) {
			System.out.println(b);
		}

		SpecialBike[] specialBikes = new SpecialBike[3];
		for (int i = 0; i < specialBikes.length; i++) {
			specialBikes[i] = new SpecialBike(i + 4, "Special Bike" + String.valueOf(i + 1), 200.0 + i, "sports", "lightweight");
		}
		for (SpecialBike sb: specialBikes) {
			System.out.println(sb);
		}

		Orderline[] orderlines = new Orderline[4];
		for (int i = 0; i < orderlines.length - 1; i++) {
			orderlines[i] = new Orderline(bikes[i], i + 1);
		}
		orderlines[3] = new Orderline(specialBikes[1], 6);
		for (Orderline ol: orderlines) {
			System.out.println(ol);
		}


		Order or = new Order(customer, "2023-12-05", orderlines);
		System.out.println(or);
		or.printOrderLine();

	}
}
