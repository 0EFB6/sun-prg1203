public class Orderline {
	private Bike bike;
	private int quantity;

	// Constructor
	public Orderline() {
	}

	public Orderline(Bike bike, int quantity) {
		this.bike = bike;
		this.quantity = quantity;
	}

	// Setters and getters
	public Bike getBike() {
		return this.bike;
	}

	public void setBike(Bike bike) {
		this.bike = bike;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	// Other Methods
	@Override
	public String toString() {
		return "Orderline [" + "bike=" + this.bike + ", quantity=" + this.quantity + "]";
	}
}
