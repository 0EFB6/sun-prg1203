public class Order {
	private Customer customer;
	private String date;
	private Orderline[] orderlines;

	// Constructor
	public Order() {
	}

	public Order(Customer customer, String date, Orderline[] orderlines) {
		this.customer = customer;
		this.date = date;
		this.orderlines = orderlines;
	}

	// Setters and getters
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Orderline[] getOrderlines() {
		return this.orderlines;
	}

	public void setOrderlines(Orderline[] orderlines) {
		this.orderlines = orderlines;
	}

	// Other Methods
	public void printOrderLine() {
		for (Orderline ol: this.orderlines) {
			System.out.println(ol);
		}
	}
	
	@Override
	public String toString() {
		return "Order [customer=" + this.customer + ", date=" + this.date + ", orderlines="
				+ this.orderlines + "]]";
	}


}
