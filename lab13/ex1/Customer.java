public class Customer {
	private int id;
	private String name;
	private String address;

	// Constructor
	public Customer() {
	}

	public Customer(int id, String name, String address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}

	// Setters and getters
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	// Other Methods
	@Override
	public String toString() {
		return "Customer [id=" + this.id + ", name=" + this.name + ", address=" + this.address + "]";
	}

	public void placeOrder(Order order, Orderline[] orderlines) {
		System.out.println("Order placed: " + order);
	}

}
