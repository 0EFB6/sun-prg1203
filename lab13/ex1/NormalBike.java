public class NormalBike extends Bike {
	// Constructor
	public NormalBike() {
	}

	public NormalBike(int id, String description, double price, String type) {
		super(id, description, price, type);
	}

	// Other Methods
	@Override
	public String toString() {
		return "NormalBike " + super.toString();
	}	
}
