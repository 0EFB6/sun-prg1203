public class studenttest {
    public static void main(String[] args) {
        // Create three Car objects and print them
        Student student1 = new Student("Alex", 5, "BCS");
        Student student2 = new Student("Mindy", 2, "BIT");

        System.out.println(student1);
        System.out.println(student2);

        student1.setName("Johnson");

        System.out.println(student1);
        System.out.println(student2);

        student1.checkEligibiligty();
        student2.checkEligibiligty();    
    }
}

