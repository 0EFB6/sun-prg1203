public class Student {
    private String name;
    private int semester;
    private String course;

    // Constructor
    public Student(String name, int semester, String course)
    {
        this.name = name;
        this.semester = semester;
        this.course = course;
    }

    // Getter and Setter methods
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getSemester()
    {
        return semester;
    }

    public void setSemester(int semester)
    {
        this.semester = semester;
    }

    public String getCourse()
    {
        return course;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }

    // Override toString method
    @Override
    public String toString()
    {
        return "Student with name " + name + " studies course " + course +
               " in semester " + semester;
    }

    // Other methods
    public void checkEligibiligty()
    {
        if (semester >= 4 && semester <= 6)
        {
            System.out.println("Student with name " + name + " studies course " + course +
               " in semester " + semester + " is eligible to get credit exemption.");
        }
        else
            System.out.println("Student with name " + name + " studies course " + course +
               " in semester " + semester + " is not eligible to get credit exemption.");
    }    
}
