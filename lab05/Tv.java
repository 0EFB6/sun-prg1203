import java.util.Scanner;

public class Tv {
    private String stockNumber;
    private String make;
    private char screenType;
    private double price;

    // Constructor
    public Tv(String stockNumber, String make, char screenType, double price)
    {
        this.stockNumber = stockNumber;
        this.make = make;
        this.screenType = screenType;
        this.price = price;
    }

    // Getter and Setter methods
    public String getStockNumber()
    {
        return stockNumber;
    }

    public void setStockNumber(String s)
    {
        this.stockNumber = s;
    }

    public String getMake()
    {
        return make;
    }

    public void setMake(String m)
    {
        this.make = m;
    }

    public char getScreenType()
    {
        return screenType;
    }

    public void setScreenType(char t)
    {
        this.screenType = t;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double p)
    {
        this.price = p;
    }

   
    // Override toString method
    @Override
    public String toString()
    {
        return "TV with stock number " + stockNumber + " with the make of " + make +
               " has a screen type of " + screenType + " and its price is " + price;
    }

    // Other methods
    public void taxRate()
    {
        double taxRate, tax;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the tax rate: ");
        taxRate = input.nextDouble();
        tax = price * (taxRate / 100);
        System.out.println("TV with stock number " + stockNumber + " with the make of " + make +
               " has a screen type of " + screenType + " and its price is " + price + " and has a tax of " + tax);
        input.close();
    }    
}
