import java.util.Scanner;
import java.lang.Math;

public class ex3
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner( System.in );
		int num;

		System.out.print("Enter integer: ");
		num = input.nextInt();

		if (num % 2 != 0)
			System.out.printf("Number is odd\n");
		else
			System.out.printf("Number is even\n");
	}
}
