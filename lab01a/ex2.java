import java.util.Scanner;
import java.lang.Math;

public class ex2
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner( System.in );
		int num, num1, num2, num3, num4, num5;

		System.out.print("Enter five digit integer: ");
		num = input.nextInt();

		if (num >= 10000 && num <= 99999)
		{
			num1 = num / 10000;
			num2 = (num / 1000) % 10;
			num3 = (num / 100) % 10;
			num4 = (num / 10) % 10;
			num5 = num % 10;
			System.out.printf("Digits in %d are %d %d %d %d %d\n", num, num1, num2, num3, num4, num5);
		}
		else
			System.out.println("Enter 5 digits integer ONLY!");
	}
}
