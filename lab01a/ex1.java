import java.util.Scanner;
import java.lang.Math;

public class ex1
{
	public static void main( String[] args )
	{
		int largest;
		int smallest;
		int sum;
		int product;
		int average;
		int num1, num2, num3;
		Scanner input = new Scanner( System.in );

		System.out.print("Enter first integer: ");
		num1 = input.nextInt();
		System.out.print("Enter second integer: ");
		num2 = input.nextInt();
		System.out.print("Enter third integer: ");
		num3 = input.nextInt();

		largest = Math.max(Math.max(num1, num2), num3);
		smallest = Math.min(Math.min(num1, num2), num3);
		sum = num1 + num2 + num3;
		product = num1 * num2 * num3;
		average = (int) sum / 3;
		

		System.out.println("\nFor the numbers 10, 20 and 30");
		System.out.printf("Largest is %d\n", largest);
		System.out.printf("Smallest is %d\n", smallest);
		System.out.printf("Sum is %d\n", sum);
		System.out.printf("Product is %d\n", product);
		System.out.printf("Average is %d\n", average);
	}
}
