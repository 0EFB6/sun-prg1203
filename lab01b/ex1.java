import java.util.Scanner;

public class ex1
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner( System.in );
        int choice;
		double product1 = 2.98;
		double product2 = 4.50;
		double product3 = 9.98;
        double total_product1 = 0;
        double total_product2 = 0;
        double total_product3 = 0;

		System.out.print("Enter product number (1-3) (0 to stop): ");
		choice = input.nextInt();
        while ( choice != 0 )
        {
            switch ( choice)
            {
                case 1:
                    total_product1 = getQuantitySold(product1, total_product1);
                    break;
                case 2:
                    total_product2 = getQuantitySold(product2, total_product2);
                    break;
                case 3:
					total_product3 = getQuantitySold(product3, total_product3);
                    break;
                default:
                    System.out.println("Invalid input");
                    break;
            }
            System.out.print("Enter product number (1-3) (0 to stop): ");
            choice = input.nextInt();
        }
        System.out.printf("\nProduct 1: $%.2f", total_product1);
        System.out.printf("\nProduct 2: $%.2f", total_product2);
        System.out.printf("\nProduct 3: $%.2f\n", total_product3);
	}

	public static double getQuantitySold(double productPrice, double total)
	{
		int quantity;
		Scanner input = new Scanner( System.in );

        System.out.print("Enter quantity sold: ");
        quantity = input.nextInt();
        total += productPrice * quantity;
		return total;
	}
}
