import java.util.Scanner;

public class InvoiceTest
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        Invoice invoice1 = new Invoice("1234", "Hammer", 2, 14.95);
        Invoice invoice2 = new Invoice("5678", "Paint Brush", -5, -9.99);

        System.out.println(invoice1);
        System.out.printf("\n");
        System.out.println(invoice2);

        invoice1.setPartNumber("001234");
        invoice1.setPartDesc("Yellow Hammer");
        invoice1.setPurchasedQuantity(3);
        invoice1.setPricePerItem(19.49);
        invoice2.setPurchasedQuantity(3);
        invoice2.setPricePerItem(9.49);

        System.out.printf("\nAfter updating:\n");
        System.out.println(invoice1);
        System.out.printf("\n");
        System.out.println(invoice2);

        input.close();
    }
}