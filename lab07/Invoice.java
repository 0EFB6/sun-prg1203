public class Invoice {
    private String partNumber;
    private String partDesc;
    private int purchasedQuantity;
    private double pricePerItem;

    // Constructor
    public Invoice()
    {
        partNumber = "";
        partDesc = "";
        purchasedQuantity = 0;
        pricePerItem = 0.0;
    }

    public Invoice(String partNumber, String partDesc, int purchasedQuantity, double pricePerItem)
    {
        setPartNumber(partNumber);
        setPartDesc(partDesc);
        setPurchasedQuantity(purchasedQuantity);
        setPricePerItem(pricePerItem);
    }

    // Getter & setter
    public void setPartNumber(String n)
    {
        this.partNumber = n;
    }

    public String getPartNumber()
    {
        return partNumber;
    }

    public void setPartDesc(String d)
    {
        this.partDesc = d;
    }

    public String getPartDesc()
    {
        return partDesc;
    }

    public void setPurchasedQuantity(int q)
    {
        if (q < 0)
            q = 0;
        this.purchasedQuantity = q;
    }

    public int getPurchasedQuantity()
    {
        return purchasedQuantity;
    }

    public void setPricePerItem(double p)
    {
        if (p < 0)
            p = 0.0;
        this.pricePerItem = p;
    }

    public double getPricePerItem()
    {
        return pricePerItem;
    }

    // Methods
    public double getInvoiceAmount()
    {
        return purchasedQuantity * pricePerItem;
    }

    // Override toString method
    @Override
    public String toString()
    {
        return String.format("Invoice1\nPart number: %s\nDescription: %s\nQuantity: %d\nPrice: %.2f\nInvoice Amount: %.2f\n", getPartNumber(), getPartDesc(), getPurchasedQuantity(),getPricePerItem() ,getInvoiceAmount());
    }
}