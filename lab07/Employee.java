public class Employee
{
    private String name;
    private double monthlySalary;
    private static double totalSalary;

    // Constructor
    public Employee()
    {
        name = "";
        setMonthlySalary(0);
    }

    public Employee(String name, double monthlySalary)
    {
        setName(name);
        setMonthlySalary(monthlySalary);
    }

    // Setter and getter
    public void setName(String n)
    {
        this.name = n;
    }

    public String getName()
    {
        return name;
    }

    public void setMonthlySalary(double s)
    {
        totalSalary = totalSalary - monthlySalary * 12;
        if (s > 0)
            monthlySalary = s;
        totalSalary = totalSalary + monthlySalary * 12;
    }

    public double getMonthlySalary()
    {
        return monthlySalary;
    }

    // Methods
    public void increaseSalary(double s)
    {
        setMonthlySalary(getMonthlySalary()*(1+s/100));
    }

    public static double getTotalSalary()
    {
        return totalSalary;
    }

    // Override toString() method
    @Override
    public String toString()
    {
        return String.format("Name: %s\n Yearly salary: %.2f\n", getName(), getMonthlySalary()*12);
    }
}
