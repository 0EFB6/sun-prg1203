import java.util.Scanner;

public class EmployeeTest
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        Employee employee1 = new Employee("John", 5000);
        Employee employee2 = new Employee("Mary", 6000);

        System.out.println(employee1);
        System.out.println(employee2);

        employee1.increaseSalary(10);
        employee2.increaseSalary(10);

        System.out.printf("\nAfter updating:\n");
        System.out.println(employee1);
        System.out.println(employee2);

        System.out.printf("\nTotal salary: %.2f\n", Employee.getTotalSalary());

        input.close();
    }
}