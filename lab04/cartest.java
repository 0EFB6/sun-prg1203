import java.util.ArrayList;

public class cartest {
    public static void main(String[] args) {
        // Create three Car objects and print them
        Car car1 = new Car("Toyota", "Red", 2, 60);
        Car car2 = new Car("Honda", "Blue", 1, 70);
        Car car3 = new Car("Ford", "Green", 3, 80);

        System.out.println(car1);
        System.out.println(car2);
        System.out.println(car3);

        // Change the speed of car1 and print it again
        car1.setSpeed(100);
        System.out.println(car1);
        System.out.println(car1.getSpeed());

        car1.accelerate();
        car2.decelerate();
        car3.stop();
        System.out.println(car1);
        System.out.println(car2);
        System.out.println(car3);

        // Store car objects in an ArrayList
        ArrayList<Car> carList = new ArrayList<>();
        carList.add(car1);
        carList.add(car2);
        carList.add(car3);

        // Print car objects in the ArrayList using a for loop
        for (Car car : carList) {
            System.out.println(car);
        }
    }
}

