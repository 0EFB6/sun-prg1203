public class Car {
    private String brand;
    private String color;
    private int engine_size;
    private int speed;

    // Constructor
    public Car(String b, String c, int e, int s) {
        this.brand = b;
        this.color = c;
        this.engine_size = e;
        this.speed = s;
    }

    // Getter and Setter methods
    public String getBrand() {
        return brand;
    }

    public void setBrand(String b) {
        this.brand = b;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String c) {
        this.color = c;
    }

    public int getEngine_size() {
        return engine_size;
    }

    public void setEngine_size(int e) {
        this.engine_size = e;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int s) {
        this.speed = s;
    }

    // Override toString method
    @Override
    public String toString() {
        return "The car with brand " + brand + ", color " + color +
               ", engine size " + engine_size + " is running at speed " + speed;
    }

    // Other methods
    public void accelerate() {
        speed++;
    }

    public void decelerate() {
        if (speed > 0) {
            speed--;
        }
    }

    public void stop() {
        speed = 0;
    }
}
